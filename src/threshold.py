import pybm3d
import os
import argparse
import numpy as np
import pandas as pd
from skimage.morphology import disk
from skimage.filters import threshold_yen, threshold_otsu, rank

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output-dir', default=None, help='path to output dir, where imgs will be stored', required=False, type=str)
    parser.add_argument('-d', '--data-file', help='path to data file', required=True, type=str)
    parser.add_argument('-m', '--meta-file', help='path to meta file', required=True, type=str)
    parser.add_argument('-e', '--end', help='max count of imgs per manual classification', required=True, type=int)
    args, unknown = parser.parse_known_args()

    visualise_thresholding(args.output_dir, args.data_file, args.meta_file, args.end)

def get_thresholding_data(threshold_function, input_data):
    thresholded_data = np.zeros(shape=input_data.shape)
    if threshold_function == 'threshold_yen':
        print('Using threshold_yen function')
        for index in range(0, len(input_data)):
            if index % 10000 == 0:
                print('{}/{}'.format(index, len(input_data)))
            thresh = threshold_yen(input_data[index])
            thresholded_data[index] = np.array(input_data[index] * (input_data[index] > thresh), dtype=np.float32)
        return thresholded_data
    elif threshold_function == 'threshold_local_otsu':
        print('Using threshold_local_otsu function')
        for index in range(0, len(input_data)):
            if index % 10000 == 0:
                print('{}/{}'.format(index, len(input_data)))
            imgc_mn = input_data[index] * (255.0 / np.max(input_data[index]))
            imgc_mn = imgc_mn.astype(np.uint8)
            thresh = rank.otsu(imgc_mn, disk(15))
            thresholded_data[index] = np.array(input_data[index] * (imgc_mn > thresh), dtype=np.float32)
        return thresholded_data

    elif threshold_function == 'threshold_global_otsu':
        print('Using threshold_global_otsu function')
        for index in range(0, len(input_data)):
            if index % 10000 == 0:
                print('{}/{}'.format(index, len(input_data)))
            thresh = threshold_otsu(input_data[index])
            thresholded_data[index] = np.array(input_data[index] * (input_data[index] > thresh), dtype=np.float32)
        return thresholded_data

    raise ValueError("threshold_function not found")

def visualise_thresholding(output_dir, data_file, meta_file, end):
    # data load
    data = np.load(data_file)
    data = data['x_y']
    df = pd.read_csv(meta_file, sep='\t')

    manual_classifications = df['manual_classification_class_name'].unique()

    if not os.path.exists('threshold_data'):
        os.makedirs('threshold_data')

    for threshold_function in ['threshold_yen', 'threshold_global_otsu', 'threshold_local_otsu']:
        thresholded_data = get_thresholding_data(threshold_function, data)
        npz_file = os.path.join('threshold_data', 'flight_{}.npz'.format(threshold_function))
        np.savez(npz_file, x_y=thresholded_data)
        for manual_classification in manual_classifications:
            df_manual_classification = df[df['manual_classification_class_name'] == manual_classification]

            count = 0

            print('Visualise {} from {} for {}'.format(end, len(df_manual_classification), manual_classification))

            for i in range(0, len(df_manual_classification), 1):
                if count > end:
                    break
                count += 1

                index = df_manual_classification['index'].values[i]

                plt.imshow(thresholded_data[index])
                plt.colorbar()
                img_dir_path = os.path.join(output_dir, threshold_function, manual_classification)

                if not os.path.exists(img_dir_path):
                    os.makedirs(img_dir_path)

                img_path = os.path.join(img_dir_path, '{}.png'.format(index))

                plt.savefig(img_path)
                plt.close('all')

    for manual_classification in manual_classifications:
        df_manual_classification = df[df['manual_classification_class_name'] == manual_classification]

        count = 0

        print('Visualise {} from {} for {}'.format(end, len(df_manual_classification), manual_classification))

        for i in range(0, len(df_manual_classification), 1):
            if count > end:
                break
            count += 1

            index = df_manual_classification['index'].values[i]

            plt.imshow(data[index])
            plt.colorbar()
            img_dir_path = os.path.join(output_dir, 'not_filtered_data', manual_classification)

            if not os.path.exists(img_dir_path):
                os.makedirs(img_dir_path)

            img_path = os.path.join(img_dir_path, '{}.png'.format(index))

            plt.savefig(img_path)
            plt.close('all')


if __name__ == '__main__':
    main()
