import os
import numpy as np
import argparse

from data_utils import normalize_input_data_from_joblib

from tensorflow.keras.models import load_model
from tensorflow.keras import Model
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output-dir', default=None, help='path to output dir, where autoencoder will be stored',
                        required=False, type=str)
    parser.add_argument('-d', '--data-file', help='path to data file', required=True, type=str)
    parser.add_argument('-n', '--normalize', help='function to normilaze data (StandardScaler, MinMaxScaler, None) default None', default=None, type=str)
    parser.add_argument('-j', '--joblib', help='path to joblib file', default=None, type=str)
    parser.add_argument('-a', '--autoencoder-file', help='path to autoencoder file', required=True, type=str)
    args, unknown = parser.parse_known_args()

    predict_autoencoder(args.output_dir, args.data_file, args.normalize, args.joblib, args.autoencoder_file)


def get_list_of_slices(chunk, num_items):
    slices = [slice(0, chunk)]
    if num_items > chunk:
        i = 1
        while num_items > chunk * i:
            slices.append(slice(chunk * i, chunk * (i + 1)))
            i += 1

    return slices


def predict_autoencoder(output_dir, data_file, normalize, joblib, autoencoder_file):
    data = np.load(data_file)

    data_xy = normalize_input_data_from_joblib(data['x_y'], normalize, joblib)

    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)

    model = load_model(autoencoder_file)
    model.summary()

    layers_names = [layer.name for layer in model.layers]
    layers_names = ['xy', 'flatten', 'reshape_2']
    #layers_names = ['xy', 'dense_1', 'reshape']

    predict_data_dir = os.path.join(output_dir, 'predict_data')
    if not os.path.exists(predict_data_dir):
        os.makedirs(predict_data_dir)

    for layer_name in layers_names:
        print('Predict for layer {}'.format(layer_name))
        intermediate_layer_model = Model(inputs=model.input, outputs=model.get_layer(layer_name).output)
        for slice_value in get_list_of_slices(5000, len(data_xy)):
            data_xy_slice = data_xy[slice_value]
            intermediate_output = intermediate_layer_model.predict(data_xy_slice)

            np_file_path = os.path.join(predict_data_dir, '{}.npy'.format(layer_name))

            if os.path.exists(np_file_path):
                x = np.load(np_file_path)
                concatenate_np = np.concatenate((x, intermediate_output), axis=0)
                np.save(np_file_path, concatenate_np)
            else:
                np.save(np_file_path, intermediate_output)

if __name__ == '__main__':
    main()
