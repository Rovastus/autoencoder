import cv2
import os
import argparse
import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output-dir', default=None, help='path to output dir, where imgs will be stored', required=False, type=str)
    parser.add_argument('-d', '--data-file', help='path to data file', required=True, type=str)
    parser.add_argument('-m', '--meta-file', help='path to meta file', required=True, type=str)
    parser.add_argument('-e', '--end', help='max count of imgs per manual classification', required=True, type=int)
    args, unknown = parser.parse_known_args()

    visualise_non_local_means(args.output_dir, args.data_file, args.meta_file, args.end)


def get_filtered_img(input_data, index, h):
    if index % 10000 == 0:
        print('{}/{}'.format(index, len(input_data)))
    data = input_data[index] * (255.0 / np.max(input_data[index]))
    data = data.astype(np.uint8)
    return cv2.fastNlMeansDenoising(data, None, h=h)


def visualise_non_local_means(output_dir, data_file, meta_file, end):
    # data load
    data = np.load(data_file)
    data = data['x_y']

    df = pd.read_csv(meta_file, sep='\t')

    manual_classifications = df['manual_classification_class_name'].unique()

    if not os.path.exists('filtered_data_non_local_means'):
        os.makedirs('filtered_data_non_local_means')

    for h in range(15, 21, 5):
        filtered_data_array = [get_filtered_img(data, x, h) for x in range(0, len(data), 1)]
        filtered_data = np.asarray(filtered_data_array)
        npz_file = os.path.join('filtered_data_non_local_means', 'flight_{}.npz'.format(h))
        np.savez(npz_file, x_y=filtered_data)
        for manual_classification in manual_classifications:
            df_manual_classification = df[df['manual_classification_class_name'] == manual_classification]

            count = 0

            print('Visualise {} from {} for {}'.format(end, len(df_manual_classification), manual_classification))

            for i in range(0, len(df_manual_classification), 1):
                if count > end:
                    break
                count += 1

                index = df_manual_classification['index'].values[i]

                plt.imshow(filtered_data[index])
                plt.colorbar()
                img_dir_path = os.path.join(output_dir, 'filtered_data_{}'.format(h), manual_classification)

                if not os.path.exists(img_dir_path):
                    os.makedirs(img_dir_path)

                img_path = os.path.join(img_dir_path, '{}.png'.format(index))

                plt.savefig(img_path)
                plt.close('all')

    for manual_classification in manual_classifications:
        df_manual_classification = df[df['manual_classification_class_name'] == manual_classification]

        count = 0

        print('Visualise {} from {} for {}'.format(end, len(df_manual_classification), manual_classification))

        for i in range(0, len(df_manual_classification), 1):
            if count > end:
                break
            count += 1

            index = df_manual_classification['index'].values[i]

            data_imshow = data[index] * (255.0 / np.max(data[index]))
            data_imshow = data_imshow.astype(np.uint8)

            plt.imshow(data_imshow)
            plt.colorbar()
            img_dir_path = os.path.join(output_dir, 'not_filtered_data', manual_classification)

            if not os.path.exists(img_dir_path):
                os.makedirs(img_dir_path)

            img_path = os.path.join(img_dir_path, '{}.png'.format(index))

            plt.savefig(img_path)
            plt.close('all')


if __name__ == '__main__':
    main()
