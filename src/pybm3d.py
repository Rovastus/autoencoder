import pybm3d
import os
import argparse
import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output-dir', default=None, help='path to output dir, where imgs will be stored', required=False, type=str)
    parser.add_argument('-d', '--data-file', help='path to data file', required=True, type=str)
    parser.add_argument('-m', '--meta-file', help='path to meta file', required=True, type=str)
    parser.add_argument('-e', '--end', help='max count of imgs per manual classification', required=True, type=int)
    args, unknown = parser.parse_known_args()

    visualise_pybm3d(args.output_dir, args.data_file, args.meta_file, args.end)


def get_filtered_img(input_data, index_start, index_end, window_size):
    if index_start % 25 == 0:
        print('{}/{}'.format(index_start, len(input_data)))
    return pybm3d.bm3d.bm3d(input_data[index_start:index_end], window_size)


def get_filtered_input_data(input_data, window_size):
    batch = 1000
    i = 0
    print(input_data.shape)
    filtered_data = np.zeros(shape=input_data.shape)
    print(filtered_data.shape)
    while i < len(input_data):
        if i % 10000 == 0:
            print('{}/{}'.format(i, len(input_data)))

        filtered_data[i:i+batch] = pybm3d.bm3d.bm3d(input_data[i:i+batch], window_size)
        i += batch

    return filtered_data


def visualise_pybm3d(output_dir, data_file, meta_file, end):
    # data load
    data = np.load(data_file)
    data = data['x_y']
    df = pd.read_csv(meta_file, sep='\t')

    manual_classifications = df['manual_classification_class_name'].unique()

    if not os.path.exists('filtered_data'):
        os.makedirs('filtered_data')

    for window_size in range(1, 3, 1):
        filtered_data = get_filtered_input_data(data, window_size)
        npz_file = os.path.join('filtered_data', 'flight_{}.npz'.format(window_size))
        if np.isnan(filtered_data).any():
            filtered_data = np.nan_to_num(filtered_data)
        np.savez(npz_file, x_y=filtered_data)
        for manual_classification in manual_classifications:
            df_manual_classification = df[df['manual_classification_class_name'] == manual_classification]

            count = 0

            print('Visualise {} from {} for {}'.format(end, len(df_manual_classification), manual_classification))

            for i in range(0, len(df_manual_classification), 1):
                if count > end:
                    break
                count += 1

                index = df_manual_classification['index'].values[i]

                plt.imshow(filtered_data[index])
                plt.colorbar()
                img_dir_path = os.path.join(output_dir, 'filtered_data_{}'.format(window_size), manual_classification)

                if not os.path.exists(img_dir_path):
                    os.makedirs(img_dir_path)

                img_path = os.path.join(img_dir_path, '{}.png'.format(index))

                plt.savefig(img_path)
                plt.close('all')

    for manual_classification in manual_classifications:
        df_manual_classification = df[df['manual_classification_class_name'] == manual_classification]

        count = 0

        print('Visualise {} from {} for {}'.format(end, len(df_manual_classification), manual_classification))

        for i in range(0, len(df_manual_classification), 1):
            if count > end:
                break
            count += 1

            index = df_manual_classification['index'].values[i]

            plt.imshow(data[index])
            plt.colorbar()
            img_dir_path = os.path.join(output_dir, 'not_filtered_data', manual_classification)

            if not os.path.exists(img_dir_path):
                os.makedirs(img_dir_path)

            img_path = os.path.join(img_dir_path, '{}.png'.format(index))

            plt.savefig(img_path)
            plt.close('all')


if __name__ == '__main__':
    main()
