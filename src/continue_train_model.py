import os
import datetime
import argparse
import numpy as np

from data_utils import normalize_input_data_from_joblib

from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.keras.models import load_model
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output-dir', default=None, help='path to output dir, where autoencoder will be stored', required=False, type=str)
    parser.add_argument('-d', '--data-file', help='path to data file', required=True, type=str)
    parser.add_argument('-m', '--model', help='path to model file', required=True, type=str)
    parser.add_argument('-n', '--normalize', help='function to normilaze data (MinMax, None) default None', default=None, type=str)
    parser.add_argument('-j', '--joblib', help='path to joblib file', default=None, type=str)
    parser.add_argument('-e', '--epochs', help='number of epochs to train the model', default=50, required=True, type=int)
    args, unknown = parser.parse_known_args()

    train_autoencoder(args.output_dir, args.data_file, args.model, args.normalize, args.joblib, args.epochs)


def train_autoencoder(output_dir, data_file, model, normalize, joblib, epochs=50):
    # data load
    data = np.load(data_file)

    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)

    data_xy = normalize_input_data_from_joblib(data['x_y'], normalize, joblib)

    autoencoder = load_model(model)
    autoencoder.summary()

    # checkpoint callback
    checkpoint_path = os.path.join(output_dir, "training_cp/cp.ckpt")

    cp_callback = ModelCheckpoint(
        filepath=checkpoint_path,
        verbose=1,
        save_weights_only=True,
        period=5
    )

    # tensorboard callback
    log_dir = os.path.join(output_dir, "logs", datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
    tb_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)

    autoencoder.fit(data_xy, data_xy, batch_size=50, epochs=epochs,
                              validation_data=(data_xy, data_xy), callbacks=[cp_callback, tb_callback])

    train_model_path = os.path.join(output_dir, 'training_model')
    if not os.path.exists(train_model_path):
        os.makedirs(train_model_path)

    train_model_file_path = os.path.join(output_dir, 'autoencoder.h5')
    autoencoder.save(train_model_file_path)

if __name__ == '__main__':
    main()