import os
import argparse
import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output-dir', default=None, help='path to output dir, where imgs will be stored', required=False, type=str)
    parser.add_argument('-d', '--data-file', help='path to data file', required=True, type=str)
    parser.add_argument('-m', '--meta-file', help='path to meta file', required=True, type=str)
    parser.add_argument('-e', '--end', help='max count of imgs per manual classification', required=True, type=int)
    args, unknown = parser.parse_known_args()

    visualise_layer(args.output_dir, args.data_file, args.meta_file, args.end)


def visualise_layer(output_dir, data_file, meta_file, end):
    # data load
    data = np.load(data_file)
    df = pd.read_csv(meta_file, sep='\t')

    manual_classifications = df['manual_classification_class_name'].unique()

    for manual_classification in manual_classifications:
        df_manual_classification = df[df['manual_classification_class_name'] == manual_classification]

        count = 0

        print('Visualise {} from {} for {}'.format(end, len(df_manual_classification), manual_classification))

        for i in range(0, len(df_manual_classification), 1):
            if count > end:
                break
            count += 1

            index = df_manual_classification['index'].values[i]

            plt.imshow(data[index])
            plt.colorbar()
            img_dir_path = os.path.join(output_dir, manual_classification)

            if not os.path.exists(img_dir_path):
                os.makedirs(img_dir_path)

            img_path = os.path.join(img_dir_path, '{}.png'.format(index))

            plt.savefig(img_path)
            plt.close('all')


if __name__ == '__main__':
    main()
