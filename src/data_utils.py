from joblib import dump, load

def normalize_input_data(input_data, normilize, joblib):
    if normilize == 'MinMax':
        print('MinMax used for input data')
        max_value = input_data.max()
        print('max value {}'.format(max_value))
        dump(max_value, joblib)
        return input_data.astype('float32') / max_value

    print('Raw data used')
    return input_data


def normalize_input_data_from_joblib(input_data, normilize, joblib):
    if normilize == 'MinMax':
        print('MinMax used for input data')
        max_value = load(joblib)
        print('max value {}'.format(max_value))
        return input_data.astype('float32') / max_value

    print('Raw data used')
    return input_data
