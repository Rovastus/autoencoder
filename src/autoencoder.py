from tensorflow.keras import Model, Input
from tensorflow.keras.regularizers import l2
from tensorflow.keras.layers import Conv2D, Conv2DTranspose, MaxPooling2D, UpSampling2D, Reshape, Flatten, Dense, BatchNormalization
from tensorflow import optimizers

def get_autoencoder(input_data):
    print('Input data shape {}'.format(input_data.shape))
    shape = tuple(input_data.shape[1:])
    encoder_input = Input(shape=shape, name='xy')

    x = Reshape(shape + (1,))(encoder_input)
    x = Conv2D(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)
    # lrn_layer
    x = Conv2D(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)

    encoder_output = Flatten()(x)

    x = Reshape((12, 12, 64))(encoder_output)
    #lrn_layer
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    # lrn_layer
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = Conv2DTranspose(1, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)

    decoder_output = Reshape(shape)(x)

    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')

    opt = optimizers.Adam(learning_rate=1e-4)
    autoencoder.compile(optimizer=opt, loss='mse', metrics=['mse'])

    autoencoder.summary()

    return autoencoder

def get_autoencoder_v1(input_data):
    print('Input data shape {}'.format(input_data.shape))
    shape = tuple(input_data.shape[1:])
    encoder_input = Input(shape=shape, name='xy')

    x = Reshape(shape + (1,))(encoder_input)
    x = Conv2D(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)
    x = Conv2D(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)
    x = Conv2D(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)
    x = Conv2D(16, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)

    encoder_output = Flatten()(x)

    x = Reshape((3, 3, 16))(encoder_output)
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(16, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = Conv2DTranspose(1, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)

    decoder_output = Reshape(shape)(x)

    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')

    opt = optimizers.Adam(learning_rate=1e-4)
    autoencoder.compile(optimizer=opt, loss='mse', metrics=['mse'])

    autoencoder.summary()

    return autoencoder

def get_autoencoder_v2(input_data):
    print('Input data shape {}'.format(input_data.shape))
    shape = tuple(input_data.shape[1:])
    encoder_input = Input(shape=shape, name='xy')

    x = Reshape(shape + (1,))(encoder_input)
    x = Conv2D(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(6)(x)
    x = Conv2D(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(4)(x)
    encoder_output = Flatten()(x)

    x = Reshape((2, 2, 64))(encoder_output)
    x = UpSampling2D(4)(x)
    x = Conv2DTranspose(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = UpSampling2D(6)(x)
    x = Conv2DTranspose(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = Conv2DTranspose(1, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)

    decoder_output = Reshape(shape)(x)

    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')

    opt = optimizers.Adam(learning_rate=1e-4)
    autoencoder.compile(optimizer=opt, loss='mse', metrics=['mse'])

    autoencoder.summary()

    return autoencoder

def get_autoencoder_v3(input_data):
    print('Input data shape {}'.format(input_data.shape))
    shape = tuple(input_data.shape[1:])
    encoder_input = Input(shape=shape, name='xy')

    x = Reshape(shape + (1,))(encoder_input)
    x = Conv2D(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)
    x = Conv2D(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)
    x = Conv2D(1, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    encoder_output = Flatten()(x)

    x = Reshape((12, 12, 1))(encoder_output)
    x = Conv2DTranspose(1, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = Conv2DTranspose(1, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)

    decoder_output = Reshape(shape)(x)

    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')

    opt = optimizers.Adam(learning_rate=1e-4)
    autoencoder.compile(optimizer=opt, loss='mse', metrics=['mse'])

    autoencoder.summary()

    return autoencoder

def get_autoencoder_v3_norm(input_data):
    print('Input data shape {}'.format(input_data.shape))
    shape = tuple(input_data.shape[1:])
    encoder_input = Input(shape=shape, name='xy')

    x = Reshape(shape + (1,))(encoder_input)
    x = Conv2D(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)
    x = Conv2D(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = MaxPooling2D(2)(x)
    x = Conv2D(1, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    encoder_output = Flatten()(x)

    x = Reshape((12, 12, 1))(encoder_output)
    x = Conv2DTranspose(1, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(64, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = UpSampling2D(2)(x)
    x = Conv2DTranspose(32, 3, strides=1, activation='relu', kernel_regularizer=l2(0.01), padding='same')(x)
    x = Conv2DTranspose(1, 3, strides=1, activation='sigmoid', kernel_regularizer=l2(0.01), padding='same')(x)

    decoder_output = Reshape(shape)(x)

    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')

    opt = optimizers.Adam(learning_rate=1e-4)
    autoencoder.compile(optimizer=opt, loss='mse', metrics=['mse'])

    autoencoder.summary()

    return autoencoder

def get_autoencoder_dense(input_data):
    print('Input data shape {}'.format(input_data.shape))
    shape = tuple(input_data.shape[1:])
    encoder_input = Input(shape=shape, name='xy')
    x = Flatten()(encoder_input)
    x = Dense(512, activation='relu')(x)
    x = Dense(256, activation='relu')(x)
    x = Dense(128, activation='relu')(x)

    x = Dense(256, activation='relu')(x)
    x = Dense(512, activation='relu')(x)
    x = Dense(input_data.shape[1] * input_data.shape[2], activation='sigmoid')(x)

    decoder_output = Reshape(shape)(x)

    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')

    opt = optimizers.Adam(learning_rate=1e-4)
    autoencoder.compile(optimizer=opt, loss='mse', metrics=['mse'])

    autoencoder.summary()

    return autoencoder

def get_autoencoder_dense_v2(input_data):
    print('Input data shape {}'.format(input_data.shape))
    shape = tuple(input_data.shape[1:])
    encoder_input = Input(shape=shape, name='xy')
    x = Flatten()(encoder_input)
    x = Dense(256, activation='relu')(x)
    x = Dense(128, activation='relu')(x)

    x = Dense(256, activation='relu')(x)
    x = Dense(shape[0] * shape[1], activation='relu')(x)
    decoder_output = Reshape(shape)(x)

    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')

    opt = optimizers.Adam(learning_rate=1e-4)
    autoencoder.compile(optimizer=opt, loss='mse', metrics=['mse'])

    autoencoder.summary()

    return autoencoder

def get_autoencoder_dense_v2_norm(input_data):
    print('Input data shape {}'.format(input_data.shape))
    shape = tuple(input_data.shape[1:])
    encoder_input = Input(shape=shape, name='xy')
    x = Flatten()(encoder_input)
    x = Dense(256, activation='relu')(x)
    x = Dense(128, activation='relu')(x)

    x = Dense(256, activation='relu')(x)
    x = Dense(shape[0] * shape[1], activation='sigmoid')(x)
    decoder_output = Reshape(shape)(x)

    autoencoder = Model(encoder_input, decoder_output, name='autoencoder')

    opt = optimizers.Adam(learning_rate=1e-4)
    autoencoder.compile(optimizer=opt, loss='mse', metrics=['mse'])

    autoencoder.summary()

    return autoencoder