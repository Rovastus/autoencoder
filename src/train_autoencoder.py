import os
import datetime
import argparse
import numpy as np

from autoencoder import get_autoencoder, get_autoencoder_v1, get_autoencoder_v2, get_autoencoder_v3
from autoencoder import get_autoencoder_dense, get_autoencoder_dense_v2
from autoencoder import get_autoencoder_v3_norm, get_autoencoder_dense_v2_norm
from data_utils import normalize_input_data

from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output-dir', default=None, help='path to output dir, where autoencoder will be stored', required=False, type=str)
    parser.add_argument('-d', '--data-file', help='path to data file', required=True, type=str)
    parser.add_argument('-n', '--normalize', help='function to normilaze data (Standard, MinMax, None) default None', default=None, type=str)
    parser.add_argument('-e', '--epochs', help='number of epochs to train the model', default=50, required=True, type=int)
    args, unknown = parser.parse_known_args()

    train_autoencoder(args.output_dir, args.data_file, args.normalize, args.epochs)


def train_autoencoder(output_dir, data_file, normalize, epochs=50):
    # data load
    data = np.load(data_file)

    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    joblib_path = os.path.join(output_dir, "normalize.joblib")
    data_xy = normalize_input_data(data['x_y'], normalize, joblib_path)

    autoencoder = get_autoencoder_v3(data_xy)

    # checkpoint callback
    checkpoint_path = os.path.join(output_dir, "training_cp/cp.ckpt")

    cp_callback = ModelCheckpoint(
        filepath=checkpoint_path,
        verbose=1,
        save_weights_only=True,
        period=5
    )

    # tensorboard callback
    log_dir = os.path.join(output_dir, "logs", datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
    tb_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)

    history = autoencoder.fit(data_xy, data_xy, batch_size=50, epochs=epochs,
                              validation_data=(data_xy, data_xy), callbacks=[cp_callback, tb_callback])

    train_model_path = os.path.join(output_dir, 'training_model')
    if not os.path.exists(train_model_path):
        os.makedirs(train_model_path)

    train_model_file_path = os.path.join(output_dir, 'autoencoder.h5')
    autoencoder.save(train_model_file_path)

    history_dict = history.history
    print(history_dict.keys())

    mse = history.history['mse']
    val_mse = history.history['val_mse']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    history_path = os.path.join(output_dir, 'history')
    if not os.path.exists(history_path):
        os.makedirs(history_path)

    np.save(os.path.join(history_path, 'mse.npy'), mse)
    np.save(os.path.join(history_path, 'val_mse.npy'), val_mse)
    np.save(os.path.join(history_path, 'loss.npy'), loss)
    np.save(os.path.join(history_path, 'val_loss.npy'), val_loss)


    # epochs = range(1, len(acc) + 1)

    # plt.plot(epochs, acc, 'bo', label='Training acc')
    # plt.plot(epochs, val_acc, 'b', label='Validation acc')
    # plt.title('Training and validation accuracy')
    # plt.legend()
    # acc_path = os.path.join(output_dir, 'acc.png')
    # plt.savefig(acc_path)
    # plt.figure()
    #
    # plt.plot(epochs, loss, 'bo', label='Training loss')
    # plt.plot(epochs, val_loss, 'b', label='Validation loss')
    # plt.title('Training and validation loss')
    # plt.legend()
    # loss_path = os.path.join(output_dir, 'loss.png')
    # plt.savefig(loss_path)

if __name__ == '__main__':
    main()