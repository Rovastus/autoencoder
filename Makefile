autoencoder:
	docker-compose -f tensorflow-docker-compose.yml run tensorflow /bin/bash

autoencoder_build:
	docker-compose -f tensorflow-docker-compose.yml build tensorflow

autoencoder_destroy:
	docker-compose -f tensorflow-docker-compose.yml kill; docker-compose -f tensorflow-docker-compose.yml rm -f ; docker volume rm $(docker volume ls -f dangling=true -q) 2> /dev/null || true
